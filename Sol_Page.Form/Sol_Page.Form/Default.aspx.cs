﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Page.Form
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PopulateControl();
        }

        private void PopulateControl()
        {

            TextBox txtName = new TextBox()
            {
                ID = "txtName",
                Width = 200,
            };

           Page.Form.Controls.Add(txtName);

            String htmlContent = "</br>";

            Page.Form.Controls.Add(new LiteralControl(htmlContent));

            Button btnObj = new Button()
            {
                ID = "btnSubmit",
                Text = "Submit"
            };



            Page.Form.Controls.Add(btnObj);

            Page.Form.Controls.Add(new LiteralControl(htmlContent));

            Label lblStatus = new Label()
            {
                ID = "lblStatus"
            };


            Page.Form.Controls.Add(lblStatus);

            //btnObj.Click += (s, e) =>
            //{

            //    lblStatus.Text = txtName.Text;
            //};

            btnObj.Click += BtnObj_Click;

            //});
        }

        private void BtnObj_Click(object sender, EventArgs e)
        {
            TextBox txtNameFind = Page.FindControl("txtName") as TextBox;
            Label lblStatusFind = Page.FindControl("lblStatus") as Label;

            lblStatusFind.Text = txtNameFind.Text;

        }
    }
}